use std::error::Error;
use std::io::BufRead;
use std::path::PathBuf;

use structopt::{
    clap::AppSettings::{ColorAuto, ColoredHelp},
    StructOpt,
};
use syntect::easy::HighlightFile;
use syntect::highlighting::{Style, Theme, ThemeSet};
use syntect::parsing::SyntaxSet;
use syntect::util::as_24_bit_terminal_escaped;

#[derive(Debug, StructOpt)]
#[structopt(setting(ColorAuto), setting(ColoredHelp), about)]
struct Cli {
    /// Verbosity level (-v, -vv, -vvv, etc.)
    /// Default (no `-v` flag(s)) => zeroth verbosity level.
    #[structopt(short, parse(from_occurrences))]
    verbosity_level: usize,

    /// Input target/path
    #[structopt(parse(from_os_str), value_name = "FILE")]
    target_path: PathBuf,
}

fn print_target_file(
    target_file: &PathBuf,
    syntax_set: &SyntaxSet,
    theme: &Theme,
) -> Result<(), Box<dyn Error>> {
    let mut parsed_file = HighlightFile::new(target_file, syntax_set, theme)?;
    let mut line = String::new();
    while parsed_file.reader.read_line(&mut line)? > 0 {
        {
            let regions: Vec<(Style, &str)> =
                parsed_file.highlight_lines.highlight(&line, &syntax_set);
            print!("{}", as_24_bit_terminal_escaped(&regions[..], false));
        }
        line.clear();
    }
    // Ref: https://docs.rs/syntect/4.4.0/syntect/util/fn.as_24_bit_terminal_escaped.html
    print!("\x1b[0m");
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Cli::from_args();
    let verbosity_level: usize = args.verbosity_level;
    if 1 < verbosity_level {
        eprint!("\n");
        dbg!(&args);
    }

    let syntax_set = SyntaxSet::load_defaults_newlines();
    let theme_set = ThemeSet::load_defaults();
    let theme = &theme_set.themes["Solarized (dark)"];
    print_target_file(&args.target_path, &syntax_set, &theme)?;

    Ok(())
}
