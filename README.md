# `rog`

A modern take on the Unix tool `cat`.
Inspired by `mog`: https://github.com/witchard/mog

## What is this?
A tool for which to replace what _I_ believe is the most typical use of `cat` (printing files to stdout) with a modern version written in Rust.

It supports:

 *  [x] Syntax highlighting
 *  [ ] Print a hex dump of binary files
 *  [ ] Show details of image files
 *  [ ] List a directory

## Install

The simplest way is to download git repo, build, and set binary on path.

